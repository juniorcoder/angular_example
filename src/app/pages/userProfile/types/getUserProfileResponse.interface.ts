import {UserProfileInterface} from 'src/app/pages/userProfile/types/userProfile.interface'

export interface GetUserProfileResponseInterface {
  profile: UserProfileInterface
}
