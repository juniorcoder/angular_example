import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { EffectsModule } from "@ngrx/effects";
import { routerReducer, StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "src/environments/environment";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ArticleModule } from "./pages/article/article.module";
import { AuthModule } from "./pages/auth/auth.module";
import { CreateArticleModule } from "./pages/createArticle/createArticle.module";
import { EditArticleModule } from "./pages/editArticle/editArticle.module";
import { GlobalFeedModule } from "./pages/globalFeed/globalFeed.module";
import { SettingsModule } from "./pages/settings/settings.module";
import { TagFeedModule } from "./pages/tagFeed/tagFeed.module";
import { UserProfileModule } from "./pages/userProfile/userProfile.module";
import { YourFeedModule } from "./pages/yourFeed/yourFeed.module";
import { TopBarModule } from "./shared/modules/topBar/topBar.module";
import { AuthInterceptor } from "./shared/services/authinterceptor.service";
import { PersistanceService } from "./shared/services/persistance.service";


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule,
    StoreModule.forRoot({router: routerReducer}),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([]),
    TopBarModule,
    GlobalFeedModule,
    YourFeedModule,
    TagFeedModule,
    CreateArticleModule,
    ArticleModule,
    EditArticleModule,
    SettingsModule,
    UserProfileModule
  ],
  providers: [
    PersistanceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
