import {AuthStateInterface} from 'src/app/pages/auth/types/authState.interface'
import {FeedStateInterface} from 'src/app/shared/modules/feed/types/feedState.interface'
import {PopularTagsStateInterface} from '../modules/popularTags/types/popularTagsState.interface'
import {ArticleStateInterface} from 'src/app/pages/article/types/articleState.interface'
import {CreateArticleStateInterface} from 'src/app/pages/createArticle/types/createArticleState.interface'
import {EditArticleStateInterface} from 'src/app/pages/editArticle/types/editArticleState.interface'
import {SettingsStateInterface} from 'src/app/pages/settings/types/settingsState.interface'
import {UserProfileStateInterface} from 'src/app/pages/userProfile/types/userProfileState.interface'

export interface AppStateInterface {
  auth: AuthStateInterface
  feed: FeedStateInterface
  popularTags: PopularTagsStateInterface
  article: ArticleStateInterface
  createArticle: CreateArticleStateInterface
  editArticle: EditArticleStateInterface
  settings: SettingsStateInterface
  userProfile: UserProfileStateInterface
}
